﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        //value1 + value2= resultのような形に持っていきたいので
        //受け皿になるvalue1を宣言
        decimal value1;
        //選ばれた演算子を受ける
        string sign;
        //結果を保存できる変数を用意
        decimal result;
        public Form1()
        {
            InitializeComponent();
            //ディスプレイの初期値
            this.textDisplay.Text = "0";
            //変数の初期化
            value1 = 0;
            result = 0;
            sign = null;
        }

        private void NumberButtonClicked(object sender, EventArgs e)
        {
           //押されたボタン情報を取得
            Button button = (Button)sender;
            //ディスプレイの表示が0の時は0を消す
            if (this.textDisplay.Text == "0")
            {
                this.textDisplay.Text = button.Text;
            }
            else
            {
                //選択されたボタンの数字をディスプレイに連結させていく
                this.textDisplay.Text =
                    this.textDisplay.Text + button.Text;
            }
            
        }

        private void CalcButtonClicked(object sender, EventArgs e)
        {
           
                //ディスプレイの初期化の前にテキストをvalue1に代入する
                value1 = decimal.Parse(this.textDisplay.Text);
                //演算子ボタンが押されたらディスプレイのテキストを初期値に戻す
                this.textDisplay.Text = "0";
            
            //どの演算子が押されたか記憶させる
            Button button = (Button)sender;
            sign = button.Text;
        }

        private void ResultButtonClicked(object sender, EventArgs e)
        {
            //どの演算子か判定
            switch(sign)
            {
                case "+":
                    result =
                        value1 + decimal.Parse(this.textDisplay.Text);
                    //計算結果をディスプレイに表示
                    this.textDisplay.Text = result.ToString();
                    break;
                case "-":
                    result =
                        value1 - decimal.Parse(this.textDisplay.Text);
                    //計算結果をディスプレイに表示
                    this.textDisplay.Text = result.ToString();
                    break;
                case "×":
                    result =
                        value1 * decimal.Parse(this.textDisplay.Text);
                    //計算結果をディスプレイに表示
                    this.textDisplay.Text = result.ToString();
                    break;
                case "÷":
                    result =
                        value1 / decimal.Parse(this.textDisplay.Text);
                    //計算結果をディスプレイに表示
                    this.textDisplay.Text = result.ToString();
                    break;
            }
        }

        private void ClearButtonClicked(object sender, EventArgs e)
        {
            //直前の入力を取り消す
            this.textDisplay.Text = "0";
        }

        private void AllClearButtonclicked(object sender, EventArgs e)
        {
            //用意している全ての変数を初期化
            this.textDisplay.Text = "0";
            value1 = 0;
            result = 0;
            sign = null;
        }

        private void DecimalPointClicded(object sender, EventArgs e)
        {
            //小数点表示にする
            this.textDisplay.Text =
                this.textDisplay.Text + this.decimalPoint.Text;
        }
    }
}
